/*
 * International Telephone Input v11.0.10
 * https://github.com/jackocnr/intl-tel-input.git
 * Licensed under the MIT license
 */

! function(a) {
    "function" == typeof define && define.amd ? define(["jquery"], function(b) {
        a(b, window, document)
    }) : "object" == typeof module && module.exports ? module.exports = a(require("jquery"), window, document) : a(jQuery, window, document)
}(function(a, b, c, d) {
    "use strict";

});
!(function( $ ) {

    /*  =========================
        Mailbox
        ========================= */

    $(function() {
        $('.go_to').click( function(){ // ловим клик по ссылке с классом go_to
            var scroll_el = $(this).attr('href'); // возьмем содержимое атрибута href, должен быть селектором, т.е. например начинаться с # или .
            if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
                $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 500); // анимируем скроолинг к элементу scroll_el
            }
            return false; // выключаем стандартное действие
        });
    });

    $(window).on('scroll', function(){
        var currentTop = $(window).scrollTop();
        if (currentTop < $('.second').offset().top) {
            $('.time').hide('fast');
        } else {
            $('.time').show('fast');
        }
    })

})(jQuery);